import { Subject } from 'rxjs';
import {Injectable} from '@angular/core';
import {Post} from '../models/Post.model';
import {Router} from '@angular/router';

@Injectable()
export class PostService {
  private posts = [
    new Post(0, 'Mon premier post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut ' +
      'labore et dolore magna aliqua. Ut enim ad minim veniam, ' +
      'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.')
  ];
  postSubject = new Subject<Post[]>();

  emitPosts() {
    this.postSubject.next(this.posts.slice());
  }

  addPost(post: Post) {
    this.posts.push(post);
    this.emitPosts();
  }

  delPost(id: number)
  {
    let i;
    if (id > -1) {
      this.posts.splice(id, 1);
      for (i=0; i <this.posts.length; i++)
      {
        this.posts[i].id=i;
      }
    }
  }

  likePost(id: number)
  {
    this.posts[id].loveIts += 1;
  }
  dislikePost(id: number)
  {
    this.posts[id].loveIts -= 1;
    this.emitPosts();
  }

  nbPosts()
  {
    return this.posts.length;
  }

  getColor(id: number) {
    if (this.posts[id].loveIts > 0) {
      return 'green';
    } else if (this.posts[id].loveIts < 0) {
      return 'red';
    }
  }
}
