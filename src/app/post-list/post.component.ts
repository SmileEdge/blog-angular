import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() postId: number;
  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postloveIts: number;
  @Input() postCreated_at: Date;
  constructor() { }

  Like() { this.postloveIts++; }
  Dislike() { this.postloveIts--; }
  getColor() {
    if (this.postloveIts > 0) {
      return 'green';
    } else if (this.postloveIts < 0) {
      return 'red';
    }
  }
  ngOnInit() {
  }

}
